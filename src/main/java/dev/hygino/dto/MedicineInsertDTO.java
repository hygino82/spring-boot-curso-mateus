package dev.hygino.dto;

import java.time.LocalDate;

public record MedicineInsertDTO(
        String name,
        String via,
        String lote,
        LocalDate expirationDate,
        Integer quantity,
        String laboratory) {
}
