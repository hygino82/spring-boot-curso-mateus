package dev.hygino.dto;

import java.time.LocalDate;

import dev.hygino.models.Medicine;

public record MedicineDTO(Long id, String name, String via, String lote, LocalDate expirationDate, Integer quantity,
		String laboratory) {

	public MedicineDTO(Medicine entity) {
		this(entity.getId(), entity.getName(), entity.getVia().toString(), entity.getLote(), entity.getExpirationDate(),
				entity.getQuantity(), entity.getLaboratory().toString());
	}
}
