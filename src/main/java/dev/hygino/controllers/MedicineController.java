package dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import dev.hygino.dto.MedicineDTO;
import dev.hygino.dto.MedicineInsertDTO;
import dev.hygino.services.MedicineService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/v1/medicine")
@RequiredArgsConstructor
public class MedicineController {

	private final MedicineService service;

	@PostMapping
	public ResponseEntity<MedicineDTO> insert(@Valid @RequestBody MedicineInsertDTO dto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.insert(dto));
	}

	@GetMapping
	public ResponseEntity<Page<MedicineDTO>> findAll(Pageable pageable) {
		Page<MedicineDTO> page = service.findAll(pageable);
		return ResponseEntity.status(HttpStatus.CREATED).body(page);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MedicineDTO> findMedicineById(@PathVariable Long id){
		return service.findMedicineById(id);
	}
	
	@DeleteMapping	("/{id}")
	public ResponseEntity<Void> remove(@PathVariable Long id) {
		return service.remove(id);
	}

}
