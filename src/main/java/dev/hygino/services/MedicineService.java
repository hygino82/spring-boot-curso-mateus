package dev.hygino.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import dev.hygino.dto.MedicineDTO;
import dev.hygino.dto.MedicineInsertDTO;
import dev.hygino.models.Medicine;
import dev.hygino.repositories.MedicineRepository;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MedicineService {

	private final MedicineRepository repository;

	public MedicineDTO insert(@Valid MedicineInsertDTO dto) {
		Medicine entity = new Medicine(dto);
		entity = repository.save(entity);

		return new MedicineDTO(entity);
	}

	public Page<MedicineDTO> findAll(Pageable pageable) {
		Page<Medicine> page = repository.findAll(pageable);

		return page.map(med -> new MedicineDTO(med));
	}

	public ResponseEntity<MedicineDTO> findMedicineById(Long id) {
		Optional<Medicine> optional = repository.findById(id);
		if (!optional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		return ResponseEntity.status(HttpStatus.OK).body(new MedicineDTO(optional.get()));
	}

	public ResponseEntity<Void> remove(Long id) {
		Optional<Medicine> optional = repository.findById(id);
		if (!optional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		repository.delete(optional.get());
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
