package dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import dev.hygino.models.Medicine;

public interface MedicineRepository extends JpaRepository<Medicine, Long>{

}
