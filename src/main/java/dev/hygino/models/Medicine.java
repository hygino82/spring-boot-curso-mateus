package dev.hygino.models;

import java.time.LocalDate;

import dev.hygino.dto.MedicineInsertDTO;
import dev.hygino.models.enums.Laboratory;
import dev.hygino.models.enums.Via;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_medicine")
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
public class Medicine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	private String name;
	private Via via;
	private String lote;
	private LocalDate expirationDate;
	private Integer quantity;
	private Laboratory laboratory;

	public Medicine(MedicineInsertDTO dto) {
		name = dto.name();
		via = Via.valueOf(dto.via().toUpperCase());
		lote = dto.lote();
		expirationDate = dto.expirationDate();
		quantity = dto.quantity();
		laboratory = Laboratory.valueOf(dto.laboratory().toUpperCase());
	}
}
