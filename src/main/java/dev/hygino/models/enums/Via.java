package dev.hygino.models.enums;

public enum Via {
	ORAL, NASAL, TOPICA, VENOSA, INTRAMUSCULAR, RETAL;
}
