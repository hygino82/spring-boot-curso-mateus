package dev.hygino.models.enums;

public enum Laboratory {
	MEDLEY, BAYER, NOVARTIS, ULTRAFARMA, ACHE;
}
